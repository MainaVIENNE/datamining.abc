Scripts permettant d'obtenir les matrices *individus-variables* qui ont servies pour le projet.

Toutes les matrices sont obtenues en utilisant les tables gene, protein, conserved_domain et functional_domain contenues dans le repertoire data.

matrice1.R : 
Permet d'obtenir la matrice 1.
Cette matrice contient les GENE_IDs filtrés de la table protein, la présence absence sur le gène de FD_ID avec family link + un certain nombre de FD_IDs aléatoire sans family link, le type du gène, la sous famille associé au gène, et la structure en domaine de la séquence protéique. 
Le nombre de FD_ID a ajouté peut etre changer directement dans le script, il est aussi possible de ne pas ajouté de FD_ID aleatoire.

matrice2.R : 
Permet d'obtenir la matrice 2.
Cette matrice contient les GENE_IDs filtrés de la table protein, des GENE_IDs aleatoire de uniquement la table gene, la présence absence sur le gène de FD_ID avec family link + un certain nombre de FD_IDs aléatoire sans family link.

matrice3.R:
Permet d'obtenir la matrice 3. 
Cette matrice contient les FD_IDs des domaines présents sur la séquence protéique, la taille du gène, la sous famille associée, le nombre de domaine transmembranaire prédit et la production d’un signal peptide. 
