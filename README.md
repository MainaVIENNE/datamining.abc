- data.preparation: contient les scripts permettant, à partir des fichiers de données fournis, d'obtenir les matrice pour l'analyse de fouille de données.

- analysis: contient le workflows ayant servi à l'analyse des matrices de données et l'évaluation des performances.

- rapport: contient le rapport sur le projet. 

- data: ce répertoire contient en théorie les fichiers CSV fournis.


